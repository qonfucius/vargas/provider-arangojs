import snakeCase from 'lodash.snakecase';
import { Database } from 'arangojs';
import { DocumentCollection, EdgeCollection } from 'arangojs/lib/cjs/collection';
import { Config } from 'arangojs/lib/cjs/connection';
import { IProvider } from 'vargasjs/lib/provider';
import Fulfillable from 'vargasjs/lib/thenables/fulfillable';
import { DOCUMENT } from 'vargasjs/lib/models/document';
import { EDGE } from 'vargasjs/lib/models/edge';
import { ICEntity, IEntity } from 'vargasjs/lib/models/entity';

type ArangoCollection = DocumentCollection | EdgeCollection;

export interface ConnectConfigInterface {
    readonly url?: string;
    readonly username?: string;
    readonly password?: string;
    readonly database?: string;
}
export default class ProviderArangoJS implements IProvider {
    private db?: Database;

    private config: ConnectConfigInterface;

    private collections: {
        [index: string]: ArangoCollection;
    } = {};

    constructor(config: ConnectConfigInterface) {
      this.config = config;
    }

    public async connect(): Promise<void> {
      const { config } = this;
      const arangoConfig: Config = {
        url: config.url,
        isAbsolute: !config.database,
      };
      this.db = new Database(arangoConfig);
      if (config.database) {
        this.db.useDatabase(config.database);
      }
      this.db.useBasicAuth(config.username, config.password);
    }

    public async isConnected(): Promise<boolean> {
      return !!this.db;
    }

    getCollection(entity: ICEntity): ArangoCollection {
      const collectionName = entity.collectionName || snakeCase(entity.name);
      if (!this.db) {
        throw new Error('Database not set');
      }
      switch (entity.type) {
        case DOCUMENT:
          return this.db.collection(collectionName);
        case EDGE:
          return this.db.edgeCollection(collectionName);
        default:
          throw new Error('Model type not recognized');
      }
    }

    // eslint-disable-next-line
    install(entity: ICEntity): void {}

    save(entity: IEntity): Fulfillable<IEntity> {
      const col = this.getCollection(entity.constructor as ICEntity);
      return Fulfillable.fromPromise<IEntity>(col.save(entity.getRaw(), { returnNew: true }), entity);
    }

    async prepare(entities: { [key: string]: ICEntity }): Promise<void> {
      await Promise.all(Object.values(entities).map(async (entity: ICEntity) => {
        try {
          await this.getCollection(entity).create();
        } catch (e) {
          if (!e.isArangoError || e.errorNum !== 1207) {
            throw e;
          }
        }
      }));
    }
}
