import Provider from '../src';

describe('Test main class', () => {
  it('Should be able to connect via uri only', async () => {
    const provider = new Provider({ url: 'arangodb://test' });
    expect(await provider.isConnected()).toBeFalsy();
    await provider.connect();
    expect(await provider.isConnected()).toBeTruthy();
  });
  it('Should be able to connect via uri & database only', async () => {
    const provider = new Provider({ url: 'arangodb://test', database: 'foo' });
    expect(await provider.isConnected()).toBeFalsy();
    await provider.connect();
    expect(await provider.isConnected()).toBeTruthy();
  });
});
